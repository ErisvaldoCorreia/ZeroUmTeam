# ====================================================
# Proposta de Estudo - Torre de Hanoi
# Criar sistema de score perfeito usando recursividade

def hanoi(n,a,b,c):
    if n==1:
    	print("move disco {} de {} para {}".format(n,a,c))
    else:
    	hanoi(n-1,a,c,b)
    	print("Move disco {} de {} para {}".format(n,a,c))
    	hanoi(n-1,b,a,c)


if __name__ == '__main__':
    response = hanoi(3, 'Pino1', 'PinoAux', 'PinoFinal')
    print(response)
